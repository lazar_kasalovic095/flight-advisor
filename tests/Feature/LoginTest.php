<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{

    use RefreshDatabase;

    public function test_user_can_login_successfully()
    {
        User::factory()->create([
            'user_name' => 'UserName',
        ]);

        $response = $this->post(route('login'), [
            'user_name' => 'UserName',
            'password' => 'secret',
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            'access_token',
            'token_type'
        ]);
    }

}
