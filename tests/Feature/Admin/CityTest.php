<?php

namespace Tests\Feature\Admin;

use App\Models\City;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CityTest extends TestCase
{

    use RefreshDatabase;

    public function test_country_id_is_required()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make([
            'country_id' => null
        ])->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertSessionHasErrors([
            'country_id' => 'The country id field is required.'
        ]);
    }

    public function test_country_id_must_exists_in_database()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make([
            'country_id' => 1000
        ])->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertSessionHasErrors([
            'country_id' => 'The selected country id is invalid.'
        ]);
    }

    public function test_city_name_is_required()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make([
            'name' => null
        ])->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertSessionHasErrors([
            'name' => 'The name field is required.'
        ]);
    }

    public function test_city_name_must_be_unique()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        City::factory()->create([
            'name' => 'Belgrade'
        ])->toArray();

        $city = City::factory()->make([
            'name' => 'Belgrade'
        ])->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertSessionHasErrors([
            'name' => 'The name has already been taken.'
        ]);
    }

    public function test_city_description_is_required()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make([
            'description' => null
        ])->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertSessionHasErrors([
            'description' => 'The description field is required.'
        ]);
    }

    public function test_city_description_must_have_two_characters()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make([
            'description' => 'a'
        ])->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertSessionHasErrors([
            'description' => 'The description must be at least 2 characters.'
        ]);
    }

    public function test_unauthorized_user_can_not_store_city()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'USER'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make()->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertStatus(401);
    }

    public function test_admin_can_successfully_store_city()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
            'role' => 'ADMIN'
        ]);

        $this->actingAs($user, 'api');

        $city = City::factory()->make()->toArray();

        $response = $this->post(route('city.store'), $city);

        $response->assertStatus(200);
    }

}
