<?php

namespace Tests\Feature;

use App\Models\City;
use App\Models\CityComment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CityCommentTest extends TestCase
{

    use RefreshDatabase;

    public function test_city_id_is_required()
    {
        $user = User::factory()->create();
        City::factory()->create();

        $this->actingAs($user, 'api');

        $comment = CityComment::factory()->make([
            'city_id' => null
        ])->toArray();

        $response = $this->post(route('cityComment.store'), $comment);

        $response->assertSessionHasErrors([
            'city_id' => 'The city id field is required.'
        ]);
    }

    public function test_city_id_must_exists_in_database()
    {
        $user = User::factory()->create();
        City::factory()->create();

        $this->actingAs($user, 'api');

        $comment = CityComment::factory()->make([
            'city_id' => 2
        ])->toArray();

        $response = $this->post(route('cityComment.store'), $comment);

        $response->assertSessionHasErrors([
            'city_id' => 'The selected city id is invalid.'
        ]);
    }

    public function test_comment_is_required()
    {
        $user = User::factory()->create();
        City::factory()->create();

        $this->actingAs($user, 'api');

        $comment = CityComment::factory()->make([
            'comment' => null
        ])->toArray();

        $response = $this->post(route('cityComment.store'), $comment);

        $response->assertSessionHasErrors([
            'comment' => 'The comment field is required.'
        ]);
    }

    public function test_comment_must_have_tree_characters()
    {
        $user = User::factory()->create();
        City::factory()->create();

        $this->actingAs($user, 'api');

        $comment = CityComment::factory()->make([
            'comment' => 'aa'
        ])->toArray();

        $response = $this->post(route('cityComment.store'), $comment);

        $response->assertSessionHasErrors([
            'comment' => 'The comment must be at least 3 characters.'
        ]);
    }

    public function test_unauthenticated_user_can_not_post_comment()
    {
        City::factory()->create();

        $comment = CityComment::factory()->make()->toArray();

        $response = $this->withHeader('Accept', 'application/json')
            ->post(route('cityComment.store'), $comment);

        $response->assertStatus(401);
    }

    public function test_authenticated_user_can_post_comment()
    {
        $user = User::factory()->create();
        City::factory()->create();

        $this->actingAs($user, 'api');

        $comment = CityComment::factory()->make()->toArray();

        $response = $this->post(route('cityComment.store'), $comment);

        $response->assertStatus(200);
    }

    public function test_admin_can_not_post_comment()
    {
        $user = User::factory([
            'role' => 'ADMIN'
        ])->create();
        City::factory()->create();

        $this->actingAs($user, 'api');

        $comment = CityComment::factory()->make()->toArray();

        $response = $this->post(route('cityComment.store'), $comment);

        $response->assertStatus(401);
    }

    public function test_user_that_was_not_owner_of_post_can_not_update_comment()
    {
        User::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user, 'api');

        City::factory()->create();
        CityComment::factory()->create();

        $response = $this->put(route('cityComment.update', ['cityComment' => 1]), [
            'comment' => 'Updated comment'
        ]);

        $response->assertStatus(403);
    }

    public function test_user_that_was_not_owner_of_post_can_not_delete_comment()
    {
        User::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user, 'api');

        City::factory()->create();
        CityComment::factory()->create();

        $response = $this->delete(route('cityComment.destroy', ['cityComment' => 1]));

        $response->assertStatus(403);
    }
//
    public function test_owner_of_comment_can_update_comment()
    {
        $user = User::factory()->create();

        $this->actingAs($user, 'api');

        City::factory()->create();
        CityComment::factory()->create();

        $response = $this->put(route('cityComment.update', ['cityComment' => 1]), [
            'comment' => 'Updated comment'
        ]);

        $response->assertStatus(200);
    }

    public function test_owner_of_comment_can_delete_comment()
    {
        $user = User::factory()->create();

        $this->actingAs($user, 'api');

        City::factory()->create();
        CityComment::factory()->create();

        $response = $this->delete(route('cityComment.destroy', ['cityComment' => 1]));

        $response->assertStatus(200);
    }

}
