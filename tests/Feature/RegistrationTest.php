<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{

    use RefreshDatabase;

    public function test_first_name_is_required()
    {
        $user = User::factory()->make([
            'first_name' => null,
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasErrors([
            'first_name' => 'The first name field is required.'
        ]);
    }

    public function test_first_name_must_have_two_characters()
    {
        $user = User::factory()->make([
            'first_name' => 'a',
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasErrors([
            'first_name' => 'The first name must be at least 2 characters.'
        ]);
    }

    public function test_last_name_is_required()
    {
        $user = User::factory()->make([
            'last_name' => null,
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasErrors([
            'last_name' => 'The last name field is required.'
        ]);
    }

    public function test_last_name_must_have_two_characters_required()
    {
        $user = User::factory()->make([
            'last_name' => 'a',
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasErrors([
            'last_name' => 'The last name must be at least 2 characters.'
        ]);
    }

    public function test_user_name_is_required()
    {
        $user = User::factory()->make([
            'user_name' => null,
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasErrors(['user_name']);
    }

    public function test_user_name_must_have_five_characters_required()
    {
        $user = User::factory()->make([
            'user_name' => 'user',
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasErrors([
            'user_name' => 'The user name must be at least 5 characters.'
        ]);
    }

    public function test_user_name_must_be_unique()
    {
        $user = User::factory()->create([
            'user_name' => 'UserName',
        ])->makeVisible(['password', 'salt']);

        $duplicatedUser = User::factory()->make([
            'user_name' => $user->user_name,
            'password' => 'secret' . '12345',
            'password_confirmation' => 'secret' . '12345'
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $duplicatedUser);

        $response->assertSessionHasErrors([
            'user_name' => 'The user name has already been taken.'
        ]);
    }

    /**
     * @return void
     */
    public function test_regular_user_can_register_successfully()
    {
        $user = User::factory()->make([
            'password' => 'secret' . '12345',
            'password_confirmation' => 'secret' . '12345'
        ])->makeVisible(['password', 'salt'])->toArray();

        $response = $this->post(route('register'), $user);

        $response->assertSessionHasNoErrors();

        $this->assertDatabaseHas('users', [
            'user_name' => $user['user_name']
        ]);
    }

}
