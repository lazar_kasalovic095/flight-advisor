# INITIALIZATION

## Requirements
- [docker](https://docs.docker.com/engine/install/ubuntu/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [composer](https://getcomposer.org/)
- mail server - for test you can use['MailTrap'](https://mailtrap.io)

## Starting project

- cp .env.example .env
- composer install
- php artisan key:generate
- ./vendor/bin/sail up -d
- ./vendor/bin/sail artisan migrate
- application will be available on http://localhost

## Running tests
- ./vendor/bin/phpunit

#### However, instead of repeatedly typing vendor/bin/sail to execute Sail commands, you may wish to configure a Bash alias that allows you to execute Sail's commands more easily:

- alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'

#### Once the Bash alias has been configured, you may execute Sail commands by simply typing sail.

# Endpoints

##### Requesting token
    POST /api/login
    Key request headers
    - Accept: application/json
    Request body: 
    - example
    {
        "user_name": "mika.zivkovic",
        "password": "secret"
    }
    
    Response on success:
    {
        "access_token": "2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx",
        "token_type": "bearer"
    }

    Response on error:
    {
        "error": "User Name or Password dos not exist."
    }

##### Registration
    POST /api/register
    Key request headers
    - Accept: application/json
    Request body: 
    - example
    {
        "first_name": "mika",
        "last_name": "zivkovic",
        "user_name": "mika.zivkovic",
        "password": "secret",
        "password_confirmation": "secret"
    }
    
    Response on success:
    {
        "message": "You are successfully registered!"
    }

    Response on error:
    {
        "message": "The given data was invalid.",
        "errors": {
            "user_name": [
                "The user name has already been taken."
            ]
        }
    }

##### Logout
    POST /api/logout
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Response on success:
    {
        "message": "Successfully logged out"
    }

    Response on error:
    {
        "message": "Unauthorized action!"
    }

##### Add City - Admin Role
    POST /api/admin/city
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Request body: 
    - example
    {
        "country_id": 1,
        "name": "Belgrade",
        "description": "Description"
    }
    
    Response on success:
    {
        "success": "City successfully added."
    }

    Response on error:
    {
        "message": "The given data was invalid.",
        "errors": {
            "name": [
                "The name has already been taken."
            ]
        }
    }

##### Import Airports and Routes - Admin Role
##### When you post files for import make sure to run [ ./vendor/bin/sail artisan queue:work ] or [ sail artisan queue:work ] if you have defined alias, because the data is imported in the background with queue jobs 
    POST /api/import
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Request body: 
    - example
    {
        [[inputs.file]]
            files = ["airports"]
            json_name_key = "airports"
            data_format = "txt"
        [[inputs.file]]
            files = ["routes"]
            json_name_key = "routes"
            data_format = "txt"
    }
    
    Response on success:
    {
        "message": "Import has started in background. You will receive an email for status."
    }

    Response on error:
    {
        "warning": "Another import is in progress"
    }

##### Find City/Cities - Regular User Role
    GET /api/city?city=Belgrade
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Response on success:
    {
        [
            {
                "id": 1604,
                "country_id": 1,
                "name": "Belgrade",
                "country": {
                    "id": 1,
                    "name": "Papua New Guinea"
                },
                "comments": [
                    {
                        "id": 1,
                        "user_id": 2,
                        "city_id": 1604,
                        "comment": "Comment 1",
                        "created_at": "2021-10-15T12:09:16.000000Z",
                        "user": {
                            "id": 2,
                            "first_name": "Mika",
                            "last_name": "Zivkovic",
                            "user_name": "mika.zivkovic"
                        }
                    }
                ]
            }
        ]
    }

##### Create City Comment - Regular User Role
    POST /api/cityComment
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Request body: 
    - example
    {
        "city_id": 1,
        "comment": "Comment"
    }
    Response on success:
    {
        [
            "success": "Comment successfully added."
        ]
    }
    Response on error:
    {
        "message": "The given data was invalid.",
        "errors": {
            "comment": [
                "The comment must be at least 3 characters."
            ]
        }
    }

##### Update City Comment - Regular User Role
    PUT|PATCH /api/cityComment/<id>
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Request body: 
    - example
    {
        "comment": "Updated Comment"
    }
    Response on success:
    {
        [
            "success": "Comment successfully updated."
        ]
    }
    Response on error:
    {
        "message": "The given data was invalid.",
        "errors": {
            "comment": [
                "The comment must be at least 3 characters."
            ]
        }
    }

##### Delete City Comment - Regular User Role
    DELETE /api/cityComment/<id>
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Response on success:
    {
        [
            "success": "Comment successfully deleted."
        ]
    }
    Response on error:
    {
        "error": "Unauthorized Action."
    }

##### Find cheapest route - Regular User Role
    GET /api/route?source_city_id=<city_id>&destination_city_id=<city_id>
    Key request headers
    - Accept: application/json
    - Authorization: Bearer 2|TJjSh2unwv14frADsaJCDwAnJpEbFKr40vb9lyUx
    Response on success:
    {
        [
            {
                "from": "Belgrade",
                "to": "London",
                "price": "24.64"
            },
            {
                "from": "London",
                "to": "Oslo",
                "price": "28.53"
            }
        ]
    }
    Response on error:
    {
        {
            "message": "The given data was invalid.",
            "errors": {
                "destination_city_id": [
                    "The selected destination city id is invalid."
                ]
            }
        }
    }
