<?php

namespace Database\Factories;

use App\Models\CityComment;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityCommentFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CityComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'city_id' => 1,
            'user_id' => 2,
            'comment' => $this->faker->sentence
        ];
    }

}
