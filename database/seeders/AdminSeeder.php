<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use App\Services\String\UniqueString;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::where(['user_name' => 'admin'])->first();

        if (!isset($admin->id)) {
            $salt = (new UniqueString())->make();

            User::create([
                'role' => 'ADMIN',
                'first_name' => 'Flight Advisor',
                'last_name' => 'Admin',
                'user_name' => 'admin',
                'password' => Hash::make('secret' . $salt),
                'salt' => $salt
            ]);
        }
    }
}
