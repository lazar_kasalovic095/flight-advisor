<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use SplFileObject;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [];

        $file = public_path() . '/airports.txt';

        $csv_file = new SplFileObject($file);
        $csv_file->setFlags(SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
        $csv_file->setCsvControl(',');

        while ($csv_file->current() !== false) {
            $line = explode(',', $csv_file->current());
            $countries[] = str_replace('"', '', $line[3]);
            $csv_file->next();
        }

        $countries = array_unique($countries, SORT_STRING);

        foreach ($countries as $country) {
            Country::create([
                'name' => $country
            ]);
        }
    }
}
