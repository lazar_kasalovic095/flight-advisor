<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('city_id');
            $table->string('iata')->comment('3-letter IATA code. Null if not assigned/unknown.');
            $table->string('icao')->comment('4-letter ICAO code. Null if not assigned.');
            $table->double('latitude');
            $table->double('longitude');
            $table->bigInteger('altitude')->comment('In feet.');
            $table->string('timezone')->comment('Hours offset from UTC.');
            $table->string('dst')
                ->comment(
                    'Daylight savings time. One of E (Europe), A (US/Canada), S (South America), O (Australia),
                    Z (New Zealand), N (None) or U (Unknown).'
                );
            $table->string('db_timezone')
                ->comment('Timezone in "tz" (Olson) format, eg. "America/Los_Angeles".');
            $table->string('type')->comment('Type of the airport.');
            $table->string('source')->comment('Source of this data.');
            $table->timestamps();

            $table->foreign('city_id')
                ->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airports');
    }
}
