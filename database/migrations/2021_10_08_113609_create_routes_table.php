<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('source_airport_id')->index();
            $table->unsignedBigInteger('destination_airport_id')->index();
            $table->string('airline');
            $table->string('airline_id');
            $table->string('code_share')->nullable();
            $table->integer('stops')->default(0)
                ->comment('Number of stops on this flight ("0" for direct).');
            $table->string('equipment')
                ->comment(
                    '3-letter codes for plane type(s) generally used on this flight, separated by spaces.'
                );
            $table->double('price')->comment('Flight cost');
            $table->timestamps();

            $table->foreign('source_airport_id')
                ->references('id')->on('airports');
            $table->foreign('destination_airport_id')
                ->references('id')->on('airports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
