<?php

namespace App\Policies;

use App\Models\CityComment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CityCommentPolicy
{

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param User $user
     * @param CityComment $cityComment
     * @return bool
     */
    public function update(User $user, CityComment $cityComment)
    {
        return $user->id === $cityComment->user_id;
    }

    /**
     * Determine if the given post can be deleted by the user.
     *
     * @param User $user
     * @param CityComment $cityComment
     * @return bool
     */
    public function destroy(User $user, CityComment $cityComment)
    {
        return $user->id === $cityComment->user_id;
    }

}
