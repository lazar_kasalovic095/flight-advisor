<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Route extends Model
{

    use HasFactory;

    /**
     * @var string[]
     */
    protected $guarded = ['id'];

    /**
     * @return HasOne
     */
    public function sourceAirport()
    {
        return $this->hasOne(Airport::class, 'id', 'source_airport_id');
    }

    /**
     * @return HasOne
     */
    public function destinationAirport()
    {
        return $this->hasOne(Airport::class, 'id', 'destination_airport_id');
    }

}
