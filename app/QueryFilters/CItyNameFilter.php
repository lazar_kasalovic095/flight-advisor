<?php

namespace App\QueryFilters;

use Closure;

class CItyNameFilter
{

    /**
     * Handle Filter
     *
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!request()->has('city') || request('city') == null) {
            return $next($request);
        }

        $builder = $next($request);

        return $builder->where(['name' => trim(request('city'))]);
    }

}
