<?php

namespace App\Jobs;

use App\Notifications\ImportFailedNotification;
use App\Services\Import\ImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;

class ProcessImportJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 7200;

    /**
     * @var string
     */
    protected $airportsFilePath;

    /**
     * @var string
     */
    protected $routesFilePath;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->airportsFilePath = '/public/import/airports.txt';
        $this->routesFilePath = '/public/import/routes.txt';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new ImportService())->handle();
    }

    /**
     * @param Throwable $exception
     *
     * @return void
     */
    public function failed(Throwable $exception)
    {
        Log::channel('import')->error($exception->getMessage());

        Notification::route('mail', config('mail.from.address'))->notify(
            new ImportFailedNotification(Str::limit($exception->getMessage()))
        );

        Storage::delete($this->airportsFilePath);
        Storage::delete($this->routesFilePath);
    }

}
