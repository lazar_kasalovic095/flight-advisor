<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AdminRoleMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->role != 'ADMIN') {
            return response()->json(['error' => 'Unauthorized Action.'], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }

}
