<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\QueryFilters\CItyNameFilter;
use Illuminate\Http\JsonResponse;
use Illuminate\Pipeline\Pipeline;

class CityController extends Controller
{

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $cities = app(Pipeline::class)
            ->send(City::query())
            ->through([
                CItyNameFilter::class,
            ])->thenReturn()
            ->select(['id', 'country_id', 'name'])
            ->with(['country' => function ($query) {
                $query->select(['id', 'name']);
            }, 'comments' => function ($query) {
                $query->select(['id', 'user_id', 'city_id', 'comment', 'created_at'])->with([
                    'user' => function ($query) {
                        $query->select(['id', 'first_name', 'last_name', 'user_name']);
                    }
                ]);
            }])
            ->get()
            ->toArray();

        return response()->json($cities, JsonResponse::HTTP_OK);
    }

}
