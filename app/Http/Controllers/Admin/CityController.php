<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CityRequest;
use App\Models\City;
use Illuminate\Http\JsonResponse;

class CityController extends Controller
{

    /**
     * @param CityRequest $request
     * @return JsonResponse
     */
    public function store(CityRequest $request)
    {
        City::create([
            'country_id' => $request->get('country_id'),
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        return response()->json(['success' => 'City successfully added.'], JsonResponse::HTTP_OK);
    }

}
