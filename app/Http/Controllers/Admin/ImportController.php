<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ImportRequest;
use App\Jobs\ProcessImportJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

class ImportController extends Controller
{

    /**
     * @param ImportRequest $request
     * @return JsonResponse
     */
    public function store(ImportRequest $request)
    {
        if (
            Storage::disk('local')->exists('public/import/airlines.txt') ||
            Storage::disk('local')->exists('public/import/routes.txt')
        ) {
            return response()->json(['warning' => 'Another import is in progress'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $request->file('airports')->storeAs('/public/import/', 'airports.txt');
        $request->file('routes')->storeAs('/public/import/', 'routes.txt');

        ProcessImportJob::dispatch();

        return response()->json([
            'message' => 'Import has started in background. You will receive an email for status.'
        ], JsonResponse::HTTP_OK);
    }

}
