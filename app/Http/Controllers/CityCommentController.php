<?php

namespace App\Http\Controllers;

use App\Http\Requests\CityCommentRequest;
use App\Models\CityComment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;

class CityCommentController extends Controller
{

    /**
     * @param CityCommentRequest $request
     * @return JsonResponse
     */
    public function store(CityCommentRequest $request)
    {
        CityComment::create([
            'user_id' => auth()->user()->id,
            'city_id' => $request->get('city_id'),
            'comment' => $request->get('comment')
        ]);

        return response()->json(['success' => 'Comment successfully added.'], JsonResponse::HTTP_OK);
    }

    /**
     * @param CityCommentRequest $request
     * @param CityComment $cityComment
     * @return JsonResponse
     */
    public function update(CityCommentRequest $request, CityComment $cityComment)
    {
//        $action = Gate::inspect('update', $cityComment);

        if (auth()->user()->id == $cityComment->user_id) {
            $cityComment->update([
                'comment' => $request->get('comment')
            ]);

            return response()->json(['success' => 'Comment successfully updated.'], JsonResponse::HTTP_OK);
        }

        return response()->json(['error' => 'Unauthorized Action.'], JsonResponse::HTTP_FORBIDDEN);
    }

    /**
     * @param CityComment $cityComment
     * @return JsonResponse
     */
    public function destroy(CityComment $cityComment)
    {
//        $action = Gate::inspect('update', $cityComment);

        if (auth()->user()->id == $cityComment->user_id) {
            $cityComment->delete();

            return response()->json(['success' => 'Comment successfully deleted.'], JsonResponse::HTTP_OK);
        }

        return response()->json(['error' => 'Unauthorized Action.'], JsonResponse::HTTP_FORBIDDEN);
    }

}
