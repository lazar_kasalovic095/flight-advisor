<?php

namespace App\Http\Controllers;

use App\Http\Requests\RouteRequest;
use App\Services\Route\RouteService;
use Illuminate\Http\JsonResponse;

class RouteController extends Controller
{

    /**
     * @param RouteRequest $request
     * @param RouteService $routeService
     * @return JsonResponse
     */
    public function index(RouteRequest $request, RouteService $routeService)
    {
        $routes = $routeService->handle();

        return response()->json($routes, JsonResponse::HTTP_OK);
    }

}
