<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Services\String\UniqueString;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    /**
     * @param RegisterRequest $request
     * @param UniqueString $uniqueString
     * @return JsonResponse
     */
    public function store(RegisterRequest $request, UniqueString $uniqueString)
    {
        $salt = $uniqueString->make();

        User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'user_name' => $request->get('user_name'),
            'password' => Hash::make($request->get('password') . $salt),
            'salt' => $salt,
        ]);

        return response()->json(['message' => 'You are successfully registered!'], JsonResponse::HTTP_OK);
    }

}
