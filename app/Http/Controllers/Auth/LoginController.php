<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    /**
     * Get a Access Token via given credentials.
     *
     * @param AuthRequest $request
     * @return JsonResponse
     */
    public function login(AuthRequest $request)
    {
        $user = User::where('user_name', $request->get('user_name'))->first();

        if (!$user || !Hash::check($request->get('password') . $user->salt, $user->password)) {
            return response()->json(['error' => 'Email or Password dos not exist.'], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return response()->json([
            'access_token' => $user->createToken('api')->plainTextToken,
            'token_type' => 'bearer'
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        if (!isset(auth()->user()->id)) {
            return response()->json(['message' => 'Unauthorized action!'], JsonResponse::HTTP_UNAUTHORIZED);
        }

        request()->user()->currentAccessToken()->delete();

        return response()->json(['message' => 'Successfully logged out'], JsonResponse::HTTP_OK);
    }

}
