<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'city_id' => 'required|exists:cities,id',
            'comment' => 'required|min:3'
        ];

        return request()->method() === 'POST'
            ? $rules
            : array_splice($rules, 1, 1);
    }
}
