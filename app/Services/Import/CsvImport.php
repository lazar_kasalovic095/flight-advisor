<?php

namespace App\Services\Import;

use Exception;
use Illuminate\Support\Collection;
use SplFileObject;

abstract class CsvImport
{

    /**
     * @var Collection
     */
    protected $items;

    /**
     * @var string
     */
    protected $filePath;

    /**
     * @var array
     */
    protected $csvHeader;

    /**
     * @var
     */
    protected $columnNumber;

    /**
     * @var string
     */
    protected $separator = ',';

    /**
     * CsvImport constructor.
     */
    public function __construct()
    {
        $this->items = collect([]);
    }

    /**
     * @return $this
     */
    public function parseFIle()
    {
        $csv_file = new SplFileObject($this->filePath);
        $csv_file->setFlags(SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
        $csv_file->setCsvControl($this->separator);

        while ($csv_file->current() !== false) {
            $line = explode($this->separator, $csv_file->current());

            foreach ($line as $key => $value) {
                $line[$key] = str_replace('"', '', $value);
            }

            if (count($line) == $this->columnNumber) {
                $this->items->add($line);
            }

            $csv_file->next();
        }

        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function validateItems()
    {
        if (!count($this->items)) {
            throw new Exception('Import file is empty!');
        }

        if (count($this->items->first()) != count($this->csvHeader)) {
            throw new Exception('Import file does`t have proper columns number!');
        }

        return $this;
    }

    /**
     * @return void
     */
    abstract function removeColumnsThatAreNotOfInterest();

    /**
     * @return void
     */
    abstract function store();

}
