<?php

namespace App\Services\Import;

use App\Notifications\ImportFailedNotification;
use App\Notifications\ImportSuccessNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class ImportService
{

    /**
     * @var AirportImport
     */
    protected $airlineImport;

    /**
     * @var string
     */
    protected $airportsFilePath;

    /**
     * @var string
     */
    protected $routesFilePath;

    /**
     * @var RouteImport
     */
    protected $routeImport;

    /**
     * ImportService constructor.
     */
    public function __construct()
    {
        ini_set('memory_limit', '2048M');

        $this->airportsFilePath = '/public/import/airports.txt';
        $this->routesFilePath = '/public/import/routes.txt';

        if (
            !Storage::disk('local')->exists($this->airportsFilePath) ||
            !Storage::disk('local')->exists($this->routesFilePath)
        ) {
            Notification::route('mail', config('mail.from.address'))->notify(
                new ImportFailedNotification('Import file/s does`t exists!')
            );

            die;
        }

        $this->airlineImport = new AirportImport($this->airportsFilePath);

        $this->routeImport = new RouteImport($this->routesFilePath);
    }

    /**
     * Import handler
     */
    public function handle()
    {
        $this->airlineImport->store();
        $this->routeImport->store();

        Storage::delete($this->airportsFilePath);
        Storage::delete($this->routesFilePath);

        Notification::route('mail', config('mail.from.address'))->notify(
            new ImportSuccessNotification()
        );
    }

}
