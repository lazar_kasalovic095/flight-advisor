<?php

namespace App\Services\Import;

use App\Models\Airport;
use App\Models\City;
use App\Notifications\ImportFailedNotification;
use Exception;
use Illuminate\Support\Facades\Notification;

class AirportImport extends CsvImport
{

    /**
     * CityImport constructor.
     *
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        parent::__construct();

        $this->columnNumber = 14;

        $this->filePath = storage_path() . '/app' . $filePath;

        $this->csvHeader = [
            'airport_id' => 0,
            'name' => 2,
            'city' => 3,
            'country' => 4,
            'iata' => 5,
            'icao' => 6,
            'latitude' => 7,
            'longitude' => 8,
            'altitude' => 8,
            'time_zone' => 9,
            'dst' => 10,
            'database_dst' => 11,
            'type' => 12,
            'source' => 13,
        ];
    }

    /**
     *
     */
    function store()
    {
        try {
            $this->parseFIle()
                ->validateItems()
                ->removeColumnsThatAreNotOfInterest();
        } catch (Exception $exception) {
            Notification::route('mail', config('mail.from.address'))->notify(
                new ImportFailedNotification($exception->getMessage())
            );

            exit;
        }

        $airports = $this->prepareDataForInsert();

        if (count($airports)) {
            $airportChunks = array_chunk($airports, 5000, true);

            foreach ($airportChunks as $airportChunk) {
                Airport::insert($airportChunk);
            }

        }
    }

    /**
     * @return $this|void
     */
    function removeColumnsThatAreNotOfInterest()
    {
        $cities = City::all(['name'])->pluck('name')->toArray();

        $this->items = $this->items->filter(function ($item) use ($cities) {
            return in_array($item[2], $cities);
        });

        Airport::chunk(3500, function ($airports) {
            $airports = $airports->pluck('id')->toArray();
            $this->items = $this->items->filter(function ($item) use ($airports) {
                return !in_array($item[0], $airports);
            });
        });

        return $this;
    }

    /**
     * @return array
     */
    private function prepareDataForInsert()
    {
        $data = [];

        $cities = City::all(['id', 'name']);

        $citiesArray = [];

        foreach ($cities as $city) {
            $citiesArray[$city->name] = $city->id;
        }

        foreach ($this->items as $item) {
            $data[] = [
                'id' => $item[0],
                'name' => $item[1],
                'city_id' => $citiesArray[$item[2]],
                'iata' => $item[4],
                'icao' => $item[5],
                'latitude' => $item[6],
                'longitude' => $item[7],
                'altitude' => $item[8],
                'timezone' => $item[9],
                'dst' => $item[10],
                'db_timezone' => $item[11],
                'type' => $item[12],
                'source' => $item[13]
            ];
        }

        return $data;
    }

}
