<?php

namespace App\Services\Import;

use App\Models\Airport;
use App\Models\Route;
use App\Notifications\ImportFailedNotification;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class RouteImport extends CsvImport
{

    /**
     * CityImport constructor.
     *
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        parent::__construct();

        $this->columnNumber = 10;

        $this->filePath = storage_path() . '/app' . $filePath;

        $this->csvHeader = [
            'airline' => 0,
            'airline_id' => 1,
            'source_airport' => 2,
            'source_airport_id' => 3,
            'destination_airport' => 4,
            'destination_airport_id' => 5,
            'code_share' => 6,
            'stops' => 7,
            'equipment' => 8,
            'price' => 9
        ];
    }

    /**
     *
     */
    function store()
    {
        try {
            $this->parseFIle()
                ->validateItems()
                ->removeColumnsThatAreNotOfInterest();
        } catch (Exception $exception) {
            Notification::route('mail', config('mail.from.address'))->notify(
                new ImportFailedNotification($exception->getMessage())
            );

            exit;
        }

        $routes = $this->prepareDataForInsert();

        if (count($routes)) {
            DB::table('routes')->truncate();

            $routeChunks = array_chunk($routes, 5000, true);

            foreach ($routeChunks as $routeChunk) {
                Route::insert($routeChunk);
            }
        }
    }

    /**
     * @return $this|void
     */
    function removeColumnsThatAreNotOfInterest()
    {
        $airports = Airport::all(['id'])->pluck('id')->toArray();

        $this->items = $this->items->filter(function ($item) use ($airports) {
            return in_array($item[$this->csvHeader['source_airport_id']], $airports)
                && in_array($item[$this->csvHeader['destination_airport_id']], $airports);
        });

        return $this;
    }

    /**
     * @return array
     */
    private function prepareDataForInsert()
    {
        $data = [];

        foreach ($this->items as $item) {
            $data[] = [
                'airline' => $item[0],
                'airline_id' => $item[1],
                'source_airport_id' => $item[3],
                'destination_airport_id' => $item[5],
                'code_share' => $item[6],
                'stops' => $item[7],
                'equipment' => $item[8],
                'price' => $item[9]
            ];
        }

        return $data;
    }

}
