<?php

namespace App\Services\Route;

use App\Models\Airport;
use App\Models\Route;
use Illuminate\Support\Facades\DB;

class RouteService
{

    /**
     * @var array
     */
    protected $routes;

    /**
     * @var
     */
    protected $sourceAirports;

    /**
     * @var
     */
    protected $destinationAirports;


    protected $directRoute;

    /**
     * RouteService constructor.
     */
    public function __construct()
    {
        $this->routes = [];

        $this->sourceAirports = Airport::where(['city_id' => request()->get('source_city_id')])->get();
        $this->destinationAirports = Airport::where(['city_id' => request()->get('destination_city_id')])->get();
    }

    public function handle()
    {
        foreach ($this->sourceAirports as $sourceAirport) {
            foreach ($this->destinationAirports as $destinationAirport) {
                $this->selectDirectRoute($sourceAirport->id, $destinationAirport->id);

                $condition = '';

                if (isset($directRoute->price)) {
                    $condition = " AND (rr.price + rp.price) < '" . $directRoute->price . "' ";
                }

                $route = DB::select(
                    "
                            WITH RECURSIVE route_paths AS
                            (
                                SELECT r.source_airport_id,
                                    concat(CAST(r.source_airport_id AS TEXT), ',',CAST(r.destination_airport_id AS TEXT)) AS path,
                                    CAST(r.id AS TEXT) AS route_ids_list, r.price, 0 AS dept
                                FROM routes r
                                WHERE r.destination_airport_id = " . $destinationAirport->id . "
                            UNION ALL
                                SELECT rr.source_airport_id, CONCAT(CAST(rr.source_airport_id AS TEXT), ',', CAST(rp.path AS TEXT)),
                                    CONCAT(CAST(rr.id AS TEXT), ',', CAST(rp.route_ids_list AS TEXT)), rr.price + rp.price,
                                    dept + 1
                                FROM routes rr
                                INNER JOIN route_paths rp ON rp.source_airport_id = rr.destination_airport_id
                                WHERE (rp.path NOT LIKE CONCAT('%',rr.source_airport_id ,'%')) " . $condition . "
                                    AND dept < 4
                            )
                            SELECT * FROM route_paths rp
                            WHERE rp.source_airport_id = " . $sourceAirport->id . "
                            ORDER BY price asc
                            LIMIT 1
            "
                );

                $this->storeFoundedRute($route);
            }
        }

        $this->sortByPrice();

        if (!count($this->routes)) {
            return [];
        }

        return $this->formatData();
    }

    /**
     * @return void
     */
    private function sortByPrice()
    {
        usort($this->routes, function ($a, $b) {
            return (float)$a['price'] > (float)$b['price'] ? 1 : 0;
        });
    }

    /**
     * @return array
     */
    private function formatData()
    {
        $paths = Route::whereIn('id', explode(',', $this->routes[0]['route_ids_list']))
            ->with(['sourceAirport.city', 'destinationAirport.city'])
            ->get();

        $data = [];

        foreach ($paths as $path) {
            $data [] = [
                'from' => $path->sourceAirport->city->name,
                'to' => $path->destinationAirport->city->name,
                'price' => $path->price
            ];
        }

        return $data;
    }

    /**
     * @param int $sourceAirportId
     * @param int $destinationAirportId
     */
    private function selectDirectRoute($sourceAirportId, $destinationAirportId)
    {
        $this->directRoute = Route::select('price')->where([
            'source_airport_id' => $sourceAirportId,
            'destination_airport_id' => $destinationAirportId
        ])->first();
    }

    /**
     * @param $route
     */
    private function storeFoundedRute($route)
    {
        if (count($route)) {
            $this->routes[] = [
                'price' => $route[0]->price,
                'route_ids_list' => $route[0]->route_ids_list
            ];
        }
    }

}
