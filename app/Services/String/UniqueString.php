<?php

namespace App\Services\String;

class UniqueString
{

    /**
     * Make unique string by time
     *
     * @return string
     */
    public function make()
    {
        return md5(uniqid(rand(), true));
    }

}
