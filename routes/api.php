<?php

use App\Http\Controllers\Admin\CityController as AdminCityController;
use App\Http\Controllers\Admin\ImportController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CityCommentController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\RouteController;
use Illuminate\Support\Facades\Route;

Route::post('login', [LoginController::class, 'login'])->name('login');
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

Route::post('register', [RegisterController::class, 'store'])->name('register');

Route::group(['middleware' => ['auth:api', 'admin'], 'prefix' => 'admin'], function () {
    Route::resource('city', AdminCityController::class)->only('store');
    Route::resource('import', ImportController::class)->only('store');
});

Route::group(['middleware' => ['auth:api', 'regular_user']], function () {
    Route::resource('route', RouteController::class)->only('index');
    Route::resource('city', CityController::class)->only('index');
    Route::resource('cityComment', CityCommentController::class)->only(['store', 'update', 'destroy']);
});
